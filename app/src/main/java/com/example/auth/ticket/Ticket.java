package com.example.auth.ticket;

import com.example.auth.app.ulctools.Commands;
import com.example.auth.app.ulctools.Utilities;
import com.google.common.primitives.Bytes;

import java.security.GeneralSecurityException;
import java.util.Arrays;

public class Ticket {

    public static final int MAX_RIDES_COUNT = 100;
    private static byte[] defaultAuthenticationKey = "BREAKMEIFYOUCAN!".getBytes();// 16-byte key

    /**
     * TODO: Change these according to your design. Diversify the keys.
     */
    private static byte[] authenticationKey = null;// 16-byte key private static byte[] masterKey =
    private static byte[] hmacMasterKey = "SECRETMASTERKEY!".getBytes(); // Our master key m [min 16-byte key]
    private static byte[] serialNumber = null;

    public static byte[] data = new byte[192];

    private static TicketMac macAlgorithm; // For computing HMAC over ticket data, as needed
    private static Utilities utils;
    private static Commands ul;

    private Boolean isValid = false;
    private int remainingUses = 0;
    private int expiryTime = 0;
    private int untilValidTime = 0;

    private static String infoToShow; // Use this to show messages in Normal Mode

    /**
     * Create a new ticket
     */
    public Ticket() throws GeneralSecurityException {
        // Set HMAC key for the ticket
        macAlgorithm = new TicketMac();
        macAlgorithm.setKey(hmacMasterKey);

        ul = new Commands();
        utils = new Utilities(ul);
    }

    /**
     * After validation, get ticket status: was it valid or not?
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * After validation, get the number of remaining uses
     */
    public int getRemainingUses() {
        return remainingUses;
    }

    /**
     * After validation, get the expiry time
     */
    public int getExpiryTime() {
        return expiryTime;
    }

    /**
     * After validation/issuing, get information
     */
    public static String getInfoToShow() {
        String tmp = infoToShow;
        infoToShow = "";
        return tmp;
    }

    /**
     * Resets the values of each field in this instance to get ready for a new ticket operation.
     */
    private void reset() {
        serialNumber = null;
        authenticationKey = null;
        untilValidTime = 0;
        expiryTime = 0;
    }

    /**
     * Issue new rides.
     */
    public boolean issue(int daysValid, int uses) throws GeneralSecurityException {
        reset();
        isValid = attemptIssueNewTicket(daysValid, uses, 5);
        return isValid;
    }

    /**
     * Use ticket once
     */
    public boolean use() throws GeneralSecurityException {
        reset();
        isValid = attemptTicketUsage();
        return isValid;
    }


    /**
     * Attempts to issue new rides on the ticket, if there are still valid rides on the card, then
     * the ride count will merely be increased.
     * @param daysValid The amount of days a new ticket would be valid. If the ride count is merely
     *                  increased, then this is not used.
     * @param uses The amount of uses to add to the card.
     * @param recoveryAttempts  The amount of remaining recovery attempts for restoring the backup.
     * @return {@code True} on success, {@code False} otherwise.
     * @throws GeneralSecurityException
     */
    private boolean attemptIssueNewTicket(int daysValid, int uses, int recoveryAttempts) throws GeneralSecurityException {
        // Authenticate
        if (utils.authenticate(getAuthenticationKey())) {
            Utilities.log("Authenticated with master key", false);
        } else if (utils.authenticate(defaultAuthenticationKey)) {
            Utilities.log("Authenticated with default key", false);

            // Apply the authentication access rules to
            final byte[] auth0 = {0x03, 0, 0, 0}; // Configuration for AUTH0: Require authentication on all writable pages.
            final byte[] auth1 = {   0, 0, 0, 0}; // Configuration for AUTH1: Neither allow read nor write operations without authentication.
            if (!utils.writePages(Bytes.concat(auth0, auth1), 0, 42, 2)) {
                Utilities.log("Failed writing authentication configuration to the card", true);
                infoToShow = "Failed to lock authentication";
                return false;
            }

            // Initialize the card's authentication key.
            if (!utils.writePages(getAuthenticationKey(), 0, 44, 4)) {
                Utilities.log("Failed writing authentication key to the card", true);
                infoToShow = "Failed to update the key";
                return false;
            }

            // Try to authenticate after writing the authentication key.
            if (utils.authenticate(getAuthenticationKey())) {
                Utilities.log("Failed to authenticate with freshly written key", true);
                infoToShow = "Failed to authenticate after updating key";
                return false;
            }

            Utilities.log("Wrote new authentication key", false);
        } else {
            Utilities.log("Authentication failed in attemptIssueNewTicket()", true);
            infoToShow = "Authentication failed";
            return false;
        }

        byte[] data = new byte[16];
        if (!readTicketData(data)) {
            Utilities.log("Couldn't read the data from the card", true);
            infoToShow = "Card couldn't be read, try again";
            return false;
        }

        TicketData readTicket = new TicketData(data);
        untilValidTime = readTicket.untilValidDate;
        expiryTime = readTicket.usableUntilDate;
        try {
            readTicket.validate();
            isValid = true;
        } catch (NoMoreRidesException e) {
            Utilities.log("No more rides: " + e.getMessage(), false);
        } catch (IllegalRidesCountException e) {
            Utilities.log("Illegal rides count: " + e.getMessage(), false);
        } catch (UseDatePassedException e) {
            Utilities.log("Date invalid: " + e.getMessage(), false);
        } catch (IncorrectMacException e) {
            Utilities.log("Mac invalid: " + e.getMessage(), false);

            byte[] backupMac = new byte[4];
            if (!utils.readPages(7, 1, backupMac, 0)) {
                Utilities.log("Couldn't read the backup data from the card", true);
                infoToShow = "Card couldn't be read, try again";
                return false;
            }

            // Try to recover from tearing
            if (readTicket.isMacMaxRidesValid(backupMac)) { // If the backup mac is still valid, copy it back.
                if (!utils.writePages(backupMac, 0, 5, 1)) {
                    Utilities.log("Torn when recovering backup", true);
                    infoToShow = "Card couldn't be written, try again";
                    return false;
                } else {
                    Utilities.log("Start recovery attempt, " + recoveryAttempts + " attempts remaining", false);
                    if (recoveryAttempts >= 0) {
                        return attemptIssueNewTicket(daysValid, uses, recoveryAttempts - 1);
                    } else {
                        infoToShow = "Card is possibly torn, try again";
                        return false;
                    }
                }
            }
        }

        int untilValidDate;
        if (isValid) { // Merely increment the ride count, as the ticket is still valid.
            remainingUses = readTicket.maxRides - readTicket.counter + uses;
            expiryTime = readTicket.usableUntilDate;
            untilValidDate = readTicket.untilValidDate;

            // Backup the MAC from page 5 to page 7
            if (!utils.writePages(data, 4, 7, 1)) {
                Utilities.log("Torn when backing up the max ride count MAC", true);
                infoToShow = "Card couldn't be written, try again";
                return false;
            }

            infoToShow = "Incremented the rides by " + uses;
        } else { // Write fresh ticket
            remainingUses = uses;
            expiryTime = 0;
            untilValidDate = daysValid + (int) Math.floor(System.currentTimeMillis() / 86400000);

            // Clean page 6
            if (!utils.writePages(new byte[4], 0, 6, 1)) {
                // If it is torn here, then no important writes have been made.
                Utilities.log("Torn when clearing date data", true);
                infoToShow = "Card couldn't be written, try again";
                return false;
            }

            infoToShow = "Issued new ticket: " + uses + " rides";
        }

        if (remainingUses > MAX_RIDES_COUNT) {
            Utilities.log("Tried to increment the count to: " + remainingUses + " > " + MAX_RIDES_COUNT, true);
            infoToShow = "Card cannot hold more than " + MAX_RIDES_COUNT + " rides";
            return false;
        }

        // Update 5 first, since this one is backup up
        byte[] remainingRidesAndUntilValidDate = Bytes.concat(intToByte(readTicket.counter + remainingUses), intToByte(untilValidDate));
        if (!utils.writePages(generateHashWithSerial(remainingRidesAndUntilValidDate), 0, 5, 1)) {
            // If it is torn here, then no important writes have been made.
            Utilities.log("Torn when writing new max rides MAC", true);
            infoToShow = "Card couldn't be written, try again";
            return false;
        }

        // Then update page 4 containing the amount of remaining rides and the until valid date.
        if (!utils.writePages(remainingRidesAndUntilValidDate, 0, 4, 1)) {
            // If it is torn here, then on next attempt will the backup be recovered.
            Utilities.log("Torn when writing the new amount of macs", true);
            infoToShow = "Card couldn't be written, try again";
            return false;
        }

        if (isValid) {
            Utilities.log("Incremented ticket count to " + remainingUses + " rides", false);
        } else {
            Utilities.log("Gave out new ticket with " + remainingUses + " rides", false);
        }
        return true;
    }

    private boolean attemptTicketUsage() throws GeneralSecurityException {
        // Authenticate
        if (utils.authenticate(getAuthenticationKey())) {
            Utilities.log("Authenticated with master key", false);
        } else if (utils.authenticate(defaultAuthenticationKey)) {
            Utilities.log("Authenticated with default key", true);
            infoToShow = "Card should first be initialized";
            return false;
        } else {
            Utilities.log("Authentication failed in use()", true);
            infoToShow = "Authentication failed";
            return false;
        }

        byte[] data = new byte[16];
        if (!readTicketData(data)) {
            Utilities.log("Couldn't read the data from the card", true);
            infoToShow = "Card couldn't be read, try again";
            return false;
        }

        System.out.print("Data: ");
        printBytes(data);

        TicketData readTicket = new TicketData(data);
        untilValidTime = readTicket.untilValidDate;
        expiryTime = readTicket.usableUntilDate;
        try {
            readTicket.validate();
        } catch (NoMoreRidesException e) {
            Utilities.log("No more rides: " + e.getMessage(), true);
            infoToShow = "No more rides on this ticket";
            return false;
        } catch (UseDatePassedException e) {
            Utilities.log("Date invalid: " + e.getMessage(), true);
            infoToShow = "Ticket expired";
            return false;
        } catch (IncorrectMacException e) {
            Utilities.log("Mac invalid: " + e.getMessage(), true);
            infoToShow = "Card data could not be read";
            return false;
        } catch (IllegalRidesCountException e) {
            Utilities.log("Illegal ride count: " + e.getMessage(), true);
            infoToShow = "Illegal ride count";
            return false;
        }

        // Check if the initial date is not already written
        if (readTicket.usableUntilDate == 0) {
            byte[] today = intToByte((int) Math.ceil(System.currentTimeMillis() / 86400000));
            byte[] todayWithMac = Bytes.concat(today, generateHashWithSerial(today));
            if (!utils.writePages(todayWithMac, 0, 6, 1)) {
                Utilities.log("Failed to pin the usage date to today", true);
                infoToShow = "Card removed too early";
                return false;
            }
            Utilities.log("Usable until date written", false);
        }

        // Lastly increment the counter to actually use the ticket.
        if (!utils.writePages(Bytes.concat(intToByte(1), intToByte(0)), 0, 41, 1)) {
            Utilities.log("Failed to increment counter", true);
            infoToShow = "Card validated, but removed too early";
            return false;
        }

        remainingUses = readTicket.maxRides - readTicket.counter - 1;
        infoToShow = "Validated! " + remainingUses + " rides remaining";
        return true;
    }

    /**
     * Reads the relevant memory segments to build a {@link TicketData} from the cards memory.
     * @param data A reference to the location where the 16 bytes should be written to.
     * @return False if reading failed, otherwise true on success.
     */
    private boolean readTicketData(byte[] data) {
        return utils.readPages(4, 3, data, 0)
                &&  utils.readPages(41, 1, data, 12);
    }

    private byte[] getSerialNumber() throws GeneralSecurityException {
        if (serialNumber == null) {
            serialNumber = new byte[8];
            if (!utils.readPages(0, 2, serialNumber, 0)) {
                serialNumber = null; // Reset serial number to the "unread" state.
                throw new GeneralSecurityException("Failed to read serial number");
            }
        }

        return serialNumber;
    }

    private byte[] getAuthenticationKey() throws GeneralSecurityException {
        if (authenticationKey == null) {
            authenticationKey = Arrays.copyOf(generateHashWithSerial(new byte[0]), 16);
            System.out.print("Authentication key: ");
            printBytes(authenticationKey);
        }

        return authenticationKey;
    }

    private void printBytes(byte[] bytes) {
        System.out.print("[");
        // Print the read serial number for logging purposes
        for (byte b : bytes) {
            System.out.print(String.format(" %02X", b));
        }
        System.out.print(" ]");
        System.out.println("length: " + bytes.length);
    }

    private byte[] intToByte(int i) {
        return new byte[]{(byte) (i & 0xff), (byte) ((i >> 8) & 0xff)};
    }

    private byte[] generateHashWithSerial(byte[] data) throws GeneralSecurityException {
        byte[] hashableData = Bytes.concat(getSerialNumber(), data);
        return macAlgorithm.generateMac(hashableData);
    }

    public int getUntilValidTime() {
        return untilValidTime;
    }

    class TicketData {
        public final int maxRides;
        public final int untilValidDate;
        public final int usableUntilDate;
        public final int counter;

        private final byte[] maxRidesBytes;
        private final byte[] untilValidDateBytes;
        private final byte[] macMaxRidesBytes;
        private final byte[] usableUntilDateBytes;
        private final byte[] macDateBytes;
        private final byte[] counterBytes;

        TicketData(byte[] data) {
            assert data.length == 16;

            maxRidesBytes = Arrays.copyOfRange(data, 0, 2);
            untilValidDateBytes = Arrays.copyOfRange(data, 2, 4);
            macMaxRidesBytes = Arrays.copyOfRange(data, 4, 8);
            usableUntilDateBytes = Arrays.copyOfRange(data, 8, 10);
            macDateBytes = Arrays.copyOfRange(data, 10, 12);
            counterBytes = Arrays.copyOfRange(data, 12, 14);

            untilValidDate = byteToInt(untilValidDateBytes[0], untilValidDateBytes[1]);
            usableUntilDate = byteToInt(usableUntilDateBytes[0], usableUntilDateBytes[1]);

            this.maxRides = byteToInt(maxRidesBytes[0], maxRidesBytes[1]);
            this.counter = byteToInt(counterBytes[0], counterBytes[1]);
        }

        public void validate() throws GeneralSecurityException {
            // First validate the amount of rides
            if (counter >= maxRides) {
                throw new NoMoreRidesException("Amount of rides depleted, c: " + counter + " max: " + maxRides);
            } else if (counter - maxRides > MAX_RIDES_COUNT) {
                throw new IllegalRidesCountException("Too many rides on this card, found " + (counter - maxRides) + " while max is " + MAX_RIDES_COUNT);
            }

            // Then the dates
            int now = (int) Math.ceil(System.currentTimeMillis() / 86400000);
            if (now > untilValidDate) {
                throw new UseDatePassedException("Until valid date has passed by " + (now - untilValidDate) + " days");
            } else if (!isMacMaxRidesValid(macMaxRidesBytes)) {
                throw new IncorrectMacException("MAC for maxRides and untilValidDate is not correct");
            }

            if (usableUntilDate != 0) {
                if (now > usableUntilDate) {
                    throw new UseDatePassedException("Usable until date has passed by " + (now - usableUntilDate) + " days");
                } else if (!hashCompare(usableUntilDateBytes, macDateBytes)) {
                    throw new IncorrectMacException("MAC for date is not correct");
                }
            }
        }

        boolean isMacMaxRidesValid(byte[] macMaxRidesBytes) throws GeneralSecurityException {
            return hashCompare(Bytes.concat(maxRidesBytes, untilValidDateBytes), macMaxRidesBytes);
        }

        private int byteToInt(byte b1, byte b2) {
            return ((b2 & 0xff) << 8) | b1 & 0xff;
        }

        private boolean hashCompare(byte[] data, byte[] expected) throws GeneralSecurityException {
            byte[] hash = generateHashWithSerial(data);
            byte[] shortenedHash = Arrays.copyOf(hash, expected.length);

            return Arrays.equals(shortenedHash, expected);
        }

    }

    class IncorrectMacException extends GeneralSecurityException {

        IncorrectMacException(String s) {
            super(s);
        }
    }

    class UseDatePassedException extends GeneralSecurityException {

        UseDatePassedException(String s) {
            super(s);
        }
    }

    class NoMoreRidesException extends GeneralSecurityException {

        NoMoreRidesException(String s) {
            super(s);
        }
    }

    class IllegalRidesCountException extends GeneralSecurityException {

        IllegalRidesCountException(String s) {
            super(s);
        }
    }
}